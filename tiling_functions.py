import cv2
from matplotlib import pyplot as plt
import numpy as np
import math

def locate_template(base, template, display_plot = False):
    
    method = cv2.TM_CCORR_NORMED  
    base_grey = cv2.cvtColor(base, cv2.COLOR_BGR2GRAY)
    image_grey = cv2.cvtColor(template, cv2.COLOR_BGR2GRAY)
    image_grey = image_grey.astype(np.uint8)
    result = cv2.matchTemplate(base_grey, image_grey, method)
    if display_plot:
        plt.imshow(result**40)
        plt.show()

    mn,mx,mnLoc,mxLoc = cv2.minMaxLoc(result)

    # Extract the coordinates of our best match
    if method in [cv2.TM_SQDIFF, cv2.TM_SQDIFF_NORMED]:
        MPx = mnLoc[0]
        MPy = mnLoc[1]
        # print(np.min(result))
    else:
        MPx = mxLoc[0]
        MPy = mxLoc[1]
        # print(np.max(result))

    # Step 2: Get the size of the template. This is the same size as the match.
    trows,tcols = image_grey.shape[:2]

    base_draw = base.copy()

    # Step 3: Draw the rectangle on large_image
    cv2.rectangle(base_draw, (MPx,MPy),(MPx+tcols,MPy+trows),(255,255,255),2)
    plt.imshow(cv2.cvtColor(base_draw, cv2.COLOR_BGR2RGB))
    plt.show()

    return MPx, MPy

def rotate(image, angle, center = None, scale = 1.0):
    (h, w) = image.shape[:2]

    if center is None:
        center = (w / 2, h / 2)

    # Perform the rotation
    M = cv2.getRotationMatrix2D(center, angle, scale)
    rotated = cv2.warpAffine(image, M, (w, h))

    return rotated

def tile(base, template, thresh = 10):
    base = cv2.cvtColor(base, cv2.COLOR_BGR2GRAY)
    template = cv2.cvtColor(template, cv2.COLOR_BGR2GRAY)
    MIN_MATCH_COUNT = thresh
    # Initiate SIFT detector
    sift = cv2.SIFT_create()
    # find the keypoints and descriptors with SIFT
    kp1, des1 = sift.detectAndCompute(base,None)
    kp2, des2 = sift.detectAndCompute(template,None)
    FLANN_INDEX_KDTREE = 1
    index_params = dict(algorithm = FLANN_INDEX_KDTREE, trees = 5)
    search_params = dict(checks = 50)
    flann = cv2.FlannBasedMatcher(index_params, search_params)
    matches = flann.knnMatch(des1,des2,k=2)
    # store all the good matches as per Lowe's ratio test.
    good = []
    for m,n in matches:
        if m.distance < 0.8*n.distance:
            good.append(m)
        
    if len(good)>= MIN_MATCH_COUNT:
        src_pts = np.float32([ kp1[m.queryIdx].pt for m in good ]).reshape(-1,1,2)
        dst_pts = np.float32([ kp2[m.trainIdx].pt for m in good ]).reshape(-1,1,2)
        M, mask = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC,5.0)
        matchesMask = mask.ravel().tolist()
        h,w = base.shape
        pts = np.float32([ [0,0],[0,h-1],[w-1,h-1],[w-1,0] ]).reshape(-1,1,2)
        dst = cv2.perspectiveTransform(pts,M)
        template = cv2.polylines(template,[np.int32(dst)],True,255,3, cv2.LINE_AA)
    else:
        print( "Not enough matches are found - {}/{}".format(len(good), MIN_MATCH_COUNT) )
        matchesMask = None

    draw_params = dict(matchColor = (0,255,0), # draw matches in green color
                   singlePointColor = None,
                   matchesMask = matchesMask, # draw only inliers
                   flags = 2)
    img3 = cv2.drawMatches(base,kp1,template,kp2,good,None,**draw_params)
    plt.imshow(img3, 'gray')
    plt.show()

    return M